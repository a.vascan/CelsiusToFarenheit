package nl.utwente.di.fahrenheitConverter;

public class Converter {

    public double convertCToF(Double temperature) {
        return temperature* 1.8d + 32.d;
    }
}
